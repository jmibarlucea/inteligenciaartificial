﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(CompositeBehaviour))]
public class CompositeBehaviourEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //setup
        CompositeBehaviour cb = (CompositeBehaviour)target;


        //check for behaviours
        if (cb._behaviours == null || cb._behaviours.Length == 0)
        {
            EditorGUILayout.HelpBox("No behaviours in array.", MessageType.Warning);
        }
        else
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Number", GUILayout.MinWidth(60f), GUILayout.MaxWidth(60f));
            EditorGUILayout.LabelField("Behaviors", GUILayout.MinWidth(60f));
            EditorGUILayout.LabelField("Weights", GUILayout.MinWidth(60f), GUILayout.MaxWidth(60f));
            EditorGUILayout.EndHorizontal();

            EditorGUI.BeginChangeCheck();

            for (int i = 0; i < cb._behaviours.Length; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(i.ToString(), GUILayout.MinWidth(60f), GUILayout.MaxWidth(60f));
                cb._behaviours[i] = (FlockBehaviour)EditorGUILayout.ObjectField(cb._behaviours[i], typeof(FlockBehaviour), false, GUILayout.MinWidth(60f));
                cb._weights[i] = EditorGUILayout.FloatField(cb._weights[i], GUILayout.MinWidth(60f), GUILayout.MaxWidth(60f));
                EditorGUILayout.EndHorizontal();
            }
            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(cb);
            }

        }
            //Add Behav
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Behviour"))
            {
                AddBehaviour(cb);
                EditorUtility.SetDirty(cb);
            }

            EditorGUILayout.EndHorizontal();

        //Remove Behav
        if (cb._behaviours != null && cb._behaviours.Length > 0)
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Remove Behviour"))
            {
                RemoveBehaviour(cb);
                EditorUtility.SetDirty(cb);
            }
            EditorGUILayout.EndHorizontal();
        }
    }

    private void AddBehaviour(CompositeBehaviour cb)
    {
        int oldCount = cb._behaviours != null ? cb._behaviours.Length : 0;
        FlockBehaviour[] newBehaviours = new FlockBehaviour[oldCount + 1];
        float[] newWeights = new float[oldCount + 1];
        for (int i = 0; i < oldCount; i++)
        {
            newBehaviours[i] = cb._behaviours[i];
            newWeights[i] = cb._weights[i];
        }

        newWeights[oldCount] = 1f;
        cb._behaviours = newBehaviours;
        cb._weights = newWeights;
    }

    private void RemoveBehaviour(CompositeBehaviour cb)
    {
        int oldCount = cb._behaviours.Length;
        if (oldCount == 1)
        {
            cb._behaviours = null;
            cb._weights = null;
            return;
        }
        else
        {
            FlockBehaviour[] newBehaviours = new FlockBehaviour[oldCount - 1];
            float[] newWeights = new float[oldCount - 1];
            for (int i = 0; i < oldCount - 1; i++)
            {
                newBehaviours[i] = cb._behaviours[i];
                newWeights[i] = cb._weights[i];
            }
            cb._behaviours = newBehaviours;
            cb._weights = newWeights;
        }
    }
}
