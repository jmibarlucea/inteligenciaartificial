﻿using System;
using System.Collections.Generic;
using UnityEditorInternal;

public class GeneticAlgorithm<T> {

    public List<DNA<T>> Population { get; private set; }
    public int Generation { get; private set; }
    public float BestFitness { get; private set; }
    public T[] BestGenes { get; private set; }

    private float _fitnessSum;

    Random random;
    List<DNA<T>> _newPopulation;


    int _dnaSize;
    Func<T> _getRandomGene;
    Func<int, float> _fitnessFunction;
    float _mutationRate;
    int _elitism;


    public GeneticAlgorithm(int populationSize, int dnaSize, Func<T> getRandomGene, Func<int,float> fitnessFunction, float mutationRate = 0.01f, int elitism = 5)
    {
        Generation = 1;

        _dnaSize = dnaSize;
        _getRandomGene = getRandomGene;
        _fitnessFunction = fitnessFunction;
        _mutationRate = mutationRate;
        _elitism = elitism;

        random = new Random();
        Population = new List<DNA<T>>(populationSize);
        _newPopulation = new List<DNA<T>>(populationSize);

        for (int i = 0; i < populationSize; i++)
        {
            Population.Add(new DNA<T>(dnaSize, getRandomGene, fitnessFunction));
        }
    }

    public void NewGeneration(int newDNA, bool crossoverNewDNA)
    {
        int finalCount = Population.Count + newDNA;

        if(finalCount <= 0)
        {
            return;
        }

        if(Population.Count > 0)
        {
            CalculateFitness();
            Population.Sort(CompareDNA);
        }

        _newPopulation.Clear();

        for (int i = 0; i < Population.Count; i++)
        {
            if (i < _elitism && i < Population.Count)
            {
                _newPopulation.Add(Population[i]);
            }
            else if(i < Population.Count || crossoverNewDNA)
            {
                DNA<T> parent1 = ChooseParent();
                DNA<T> parent2 = ChooseParent();

                DNA<T> child = parent1.Crossover(parent2);

                child.Mutate(_mutationRate);

                _newPopulation.Add(child);
            }

            else
            {
                _newPopulation.Add(new DNA<T>(_dnaSize, _getRandomGene, _fitnessFunction, shouldInitGenes: true));
            }
        }

        List<DNA<T>> tmpList = Population;
        Population = _newPopulation;
        _newPopulation = tmpList;
        Generation++;

    }

    public int CompareDNA(DNA<T> a, DNA<T> b)
    {
        if(a.Fitness > b.Fitness)
        {
            return -1;
        }
        else if(a.Fitness < b.Fitness)
        {
            return 1;
        }
        
        return 0;
    }

    private void CalculateFitness()
    {
        _fitnessSum = 0;
        DNA<T> best = Population[0];

        for (int i = 0; i < Population.Count; i++)
        {
            _fitnessSum += Population[i].CalculateFitness(i);

            if (Population[i].Fitness > best.Fitness)
                best = Population[i];
        }

        BestFitness = best.Fitness;
        best.Genes.CopyTo(BestGenes, 0);
    }

    private DNA<T> ChooseParent()
    {
        double randomNumber = random.NextDouble() * _fitnessSum;

        for (int i = 0; i < Population.Count; i++)
        {
            if(randomNumber > Population[i].Fitness)
            {
                return Population[i];
            }

            randomNumber -= Population[i].Fitness;
        }

        return null;
    }
}
