﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour {

    public Color _color;

    public Boid _boidPrefab;
    List<Boid> _boids = new List<Boid>();
    public FlockBehaviour _behaviour;
    public ContextFilter _filter;

    [SerializeField] bool _filterFlock = false;

    [Range(10, 500)]
    public int _startingCount = 250;

    const float _agentDensity = 0.08f;

    [Range(1f, 100f)]
    public float _driveFactor = 10f;

    [Range(1f, 100f)]
    public float _maxSpeed = 5f;

    [Range(1f, 10f)]
    public float _neighbourRadius = 1.5f;

    [Range(0f, 10f)]
    public float _avoidanceRadius = 0.5f;

    float _squareMaxSpeed;
    float _squareNeighbourRadius;
    float _squareAvoidanceRadius;

    public float SquareAvoidanceRadius { get { return _squareAvoidanceRadius; } }

    private void Start()
    {
        _squareMaxSpeed = _maxSpeed * _maxSpeed;
        _squareNeighbourRadius = _neighbourRadius * _neighbourRadius;
        _squareAvoidanceRadius = _avoidanceRadius * _avoidanceRadius;

        for (int i = 0; i < _startingCount; i++)
        {
            Boid newBoid = Instantiate(_boidPrefab,
                Random.insideUnitSphere * _startingCount * _agentDensity,
                Quaternion.Euler(Vector3.forward * Random.Range(0,360)),
                transform);

            newBoid.name = "Boid " + i;
            newBoid.SetFlock(this);
            newBoid.GetComponentInChildren<SkinnedMeshRenderer>().material.color = _color;
            _boids.Add(newBoid);    
        }
    }
        
    private void Update()
    {
        MoveBoids();
    }

    void MoveBoids()
    {
        foreach (Boid boid in _boids)
        {
            List<Transform> context = GetNearbyObjects(boid);

            Vector3 move = _behaviour.CalculateMove(boid, context, this);
            move *= _driveFactor;
            if (move.sqrMagnitude > _squareMaxSpeed)
            {
                move = move.normalized * _maxSpeed;
            }
            boid.Move(move);
        }
    }

    List<Transform> GetNearbyObjects(Boid boid)
    {
        List<Transform> context = new List<Transform>();

        Collider[] neighbours = Physics.OverlapSphere(boid.transform.position, _neighbourRadius);

        foreach(Collider collider in neighbours)
        {
            if(collider != boid.Collider)
            {
                context.Add(collider.transform);
            }
        }

        return _filter != null && _filterFlock ? _filter.Filter(boid, context) : context;
    }


    public void SetFiltering()
    {
        _filterFlock = !_filterFlock;
    }
}
