﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Filter/Same Flock")]
public class SameFlockFilter : ContextFilter
{
    public override List<Transform> Filter(Boid agent, List<Transform> originalContext)
    {
        List<Transform> filtered = new List<Transform>();

        foreach (Transform item in originalContext)
        {
            Boid itemAgent = item.GetComponent<Boid>();
            if(itemAgent != null && itemAgent.Flock == agent.Flock)
            {
                filtered.Add(item);
            }
        }

        return filtered;
    }
}
