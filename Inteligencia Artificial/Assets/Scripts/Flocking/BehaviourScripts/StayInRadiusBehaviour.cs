﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/Stay In Radius")]
public class StayInRadiusBehaviour : FlockBehaviour
{

    public Vector3 _center;
    public float _radius;

    public override Vector3 CalculateMove(Boid agent, List<Transform> context, Flock flock)
    {
        Vector3 centerOffeset = _center - agent.transform.position;
        float t = centerOffeset.magnitude / _radius;
        
        if(t < 0.9)
        {
            return Vector3.zero;
        }



        return centerOffeset * t * t;
    }
}
