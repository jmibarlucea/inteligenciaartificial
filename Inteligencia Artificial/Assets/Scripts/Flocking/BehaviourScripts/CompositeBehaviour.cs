﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/Composite")]
public class CompositeBehaviour : FlockBehaviour
{

    public FlockBehaviour[] _behaviours;
    public float[] _weights;

    public override Vector3 CalculateMove(Boid agent, List<Transform> context, Flock flock)
    {
        Vector3 move = Vector3.zero;

        //iterate through behaviours

        for (int i = 0; i < _behaviours.Length; i++)
        {
            Vector3 partialMove = _behaviours[i].CalculateMove(agent, context, flock) * _weights[i];

            if(partialMove != Vector3.zero)
            {
                if(partialMove.sqrMagnitude > _weights[i] * _weights[i])
                {
                    partialMove.Normalize();
                    partialMove *= _weights[i];
                }

            }

            move += partialMove;
        }

        return move;
    }
}
