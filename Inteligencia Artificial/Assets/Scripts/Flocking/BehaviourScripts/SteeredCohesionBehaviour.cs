﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Flock/Behaviour/SteeredCohesion")]
public class SteeredCohesionBehaviour : FlockBehaviour
{

    Vector3 _currentVelocity;
    public float _agentSmoothTime = 0.5f;

    public override Vector3 CalculateMove(Boid agent, List<Transform> context, Flock flock)
    {
        //if no neighbours, return no adjustments
        if (context.Count == 0)
            return Vector3.zero;

        //add all points together and average
        Vector3 cohesionMove = Vector3.zero;
        foreach (Transform item in context)
        {
            cohesionMove += item.position;
        }
        cohesionMove /= context.Count;

        //offset from agent position

        cohesionMove -= agent.transform.position;
        cohesionMove = Vector3.SmoothDamp(agent.transform.forward, cohesionMove, ref _currentVelocity, _agentSmoothTime);

        return cohesionMove;
    }
}
