﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Boid : MonoBehaviour {

    Collider _collider;
    public Collider Collider { get { return _collider; } }
    public Flock Flock { get; private set; }

    private void Start()
    {
        _collider = GetComponent<Collider>();
    }

    public void Move(Vector3 velocity)
    {
        transform.forward = velocity;
        transform.position += velocity * Time.deltaTime;
    }

    public void SetFlock(Flock flock)
    {
        Flock = flock;
    }


}
