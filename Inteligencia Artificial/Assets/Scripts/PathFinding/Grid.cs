﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {
	
	public Node[,] grid;
	public GameObject tile;
    public int rows;
    public int columns;
    public GameObject searcher;
    public static Grid instance;

    private void Awake()
    {
        if(!instance)
            instance = this;
    }
    void Start()
	{
		grid = new Node[rows,columns];
		CreateGrid();    
	}

	public void CreateGrid()
    { 
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < columns; j++)
			{
				GameObject nodeObj = Instantiate(tile,new Vector3(i,-1,j),Quaternion.identity);;
				Node node = nodeObj.GetComponent<Node>();
                node.setPos(j, i);
				grid[i, j] = node;
			}
		}

		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < columns; j++)
			{
				if(i>0)
					grid[i, j].Adj.Add(grid[i - 1, j]);
				
				if(i<rows-1)
					grid[i, j].Adj.Add(grid[i + 1, j]);
				
				if(j>0)
					grid[i, j].Adj.Add(grid[i, j - 1]);
				
				if(j<columns-1)
					grid[i, j].Adj.Add(grid[i, j + 1]);

              
                
                if(i<rows-1 && j<columns-1)
                    grid[i, j].Adj.Add(grid[i + 1, j + 1]);

                if (i > 0 && j > 0)
                    grid[i, j].Adj.Add(grid[i - 1, j - 1]);

               
                if (i > 0 && j < columns - 1)
                    grid[i, j].Adj.Add(grid[i - 1, j + 1]);

                if (i < rows - 1 && j > 0)
                    grid[i, j].Adj.Add(grid[i + 1, j - 1]);
                
            }
		}

        //CreatePlayer();
	}

    public void CreatePlayer()
    {
        int n = Random.Range(0, rows);
        int m = Random.Range(0, columns);
        Instantiate(searcher, new Vector3(n, 0, m), Quaternion.identity);
    }

   public void Clear()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                grid[i, j].setParent(null);
                grid[i, j].currentWeight = 0;
                grid[i, j].ShowWeight();
            }
        }
    }

    public void ClearColor()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                grid[i, j].material.color = Color.white;
                grid[i, j].isUsed = false;
            }
        }
    }

    public void Randomize()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                grid[i, j].weight = Random.Range(0, 10);
                grid[i, j].currentWeight = 0;
                grid[i, j].ShowWeight();
            }
        }
    }

    public void ActivateText()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                grid[i, j].ActivateText();
            }
        }
    }

    public void ShowWeight()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                grid[i, j].ShowWeight();
            }
        }
    }
}
