﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PathfinderManager : MonoBehaviour {

    enum State
    {
        Ready,
        Executing,
        Done
    }

    private Node start;
    private Node end;
    private State state = State.Ready;
    public LayerMask floorMask;
    public LayerMask searcherMask;
    public Searcher searcher;
    public Dropdown pfOption;
    Pathfinder pf;

    private void Start()
    {
        pf = new BreadthFirst();
    }

    private void Update()
    {
        PathfindSearcher();
        PathfindColor();
    }


    void MoveSearcher()
    {
        /*
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Stack<Node> path;
        if (Physics.Raycast(ray,out hit,floorMask))
        {
            path = pf.FindPath(searcher.node, hit.transform.gameObject.GetComponent<Node>());
            Debug.Log(path.Count);
            if(path != null)
            {
                searcher.Move(path);
            }
        }
        */
    }

    public void CheckPathfinder()
    {
        switch (pfOption.value)
        {
            case 0:
                pf = new BreadthFirst();
                break;
            case 1:
                pf = new Dijkstra();          
                break;
            case 2:
                pf = new AStar();
                break;
            default:
                break;
        }
    }

    void PathfindSearcher()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, searcherMask))
            {
                searcher = hit.transform.gameObject.GetComponent<Searcher>();
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse1) && searcher)
        {
            MoveSearcher();
        }
    }

    void PathfindColor()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0) && state == State.Ready)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray,out hit,floorMask))
            {
                if(start)
                    start.gameObject.GetComponent<Renderer>().material.color = Color.white;
                start = hit.transform.gameObject.GetComponent<Node>();
                start.gameObject.GetComponent<Renderer>().material.color = Color.blue;
                start.weight = 0;
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse1) && state == State.Ready)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, floorMask))
            {
                if(end)
                    end.gameObject.GetComponent<Renderer>().material.color = Color.white;
                end = hit.transform.gameObject.GetComponent<Node>();
                end.gameObject.GetComponent<Renderer>().material.color = Color.grey;
                end.weight = 0;
            }
        }


        if (Input.GetKeyDown(KeyCode.Mouse2) && state == State.Ready)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, floorMask))
            {

                Node n = hit.transform.gameObject.GetComponent<Node>();
                if (n != start && n != end)
                {
                    hit.transform.gameObject.GetComponent<Renderer>().material.color = Color.black;
                    n.isUsed = !n.isUsed;
                    if (n.isUsed)
                        n.material.color = Color.black;
                    else
                        n.material.color = Color.white;
                }
            }
        }

        if (start && end && state == State.Ready)
        {
            StartCoroutine(ColorNodes());
            
        }
    }

    public IEnumerator ColorNodes()
    {
        state = State.Executing;
        Stack<Node> path = pf.FindPath(start, end);
        foreach(Node n in pf.closedNodes)
        {
            if(n != start && n != end)
                n.material.color = Color.yellow;

            n.ShowWeight();
            yield return new WaitForSeconds(0.0005f);
        }

        if (path == null)
        {
            foreach (Node n in pf.closedNodes)
            {
                if (n != start && n != end)
                    n.material.color = Color.red;
            }
        }

        else
        {
            while (path.Count > 0)
            {
                Node n = path.Pop();
                if (n != end)
                    n.material.color = Color.green;
                yield return new WaitForSeconds(0.05f);
            }
        }
        start = null;
        end = null;
        state = State.Done;

    }

    void Clear()
    {
        if (state != State.Done)
            return;

        state = State.Executing;

        Grid.instance.Clear();
        Grid.instance.ClearColor();
        state = State.Ready;
    }

    public void Randomize()
    {
        if(state == State.Ready)
            Grid.instance.Randomize();
    }
}
