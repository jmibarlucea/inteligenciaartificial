﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;       

public class Node : MonoBehaviour
{
    public Node father = null;
    public List<Node> Adj = new List<Node>();
    public int col;
    public int row;
    public Vector3 pos;
    public bool isUsed = false;
    public Material material;
    public Text weightText;
    public int weight;
    public int currentWeight = 0;

    private void Start()
    {
        material = gameObject.GetComponent<Renderer>().material;
        weight = Random.Range(0, 10);
        
    }

    public void setPos(int _row, int _column)
    {
        row = _row;
        col = _column;
    }

    public void setParent(Node node)
    {
        father = node;
    }

    public void ActivateText()
    {
        weightText.gameObject.SetActive(!weightText.gameObject.activeInHierarchy);
        ShowWeight();
    }

    public void ShowWeight()
    {
        weightText.text = weight.ToString();
    }

    public void Update()
    {
    }

    private void OnMouseOver()
    {
        weightText.text = currentWeight.ToString();
    }

    private void OnMouseExit()
    {
        weightText.text = weight.ToString();
    }
}
