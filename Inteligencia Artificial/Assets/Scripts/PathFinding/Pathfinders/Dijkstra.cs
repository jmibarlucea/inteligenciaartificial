﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dijkstra : Pathfinder
{
    List<Node> openNodesList = new List<Node>();
    int validWeight = 0;
    Node valid = null;

    public override bool VisitNode(Node node)
    {
        if(node == endNode)
        {
            SetPath(node);
            return true;
        }

        foreach(Node n in node.Adj)
        {
            if (!openNodesList.Contains(n) && !closedNodes.Contains(n) && !n.isUsed)
            {
                n.setParent(node);
                OpenNode(n);
                n.currentWeight = node.currentWeight + n.weight;
                n.ShowWeight();
            }
        }

        CloseNode(node);

        return false;

    }

    public override Stack<Node> FindPath(Node start, Node end)
    {
        Clear();
        startNode = start;
        endNode = end;
        OpenNode(start);

        while (openNodesList.Count > 0)
        {
            int aux = int.MaxValue;
            foreach(Node n in openNodesList)
            {
                if(validWeight + n.currentWeight < aux)
                {
                    aux = n.currentWeight + validWeight;
                    valid = n;
                }
            }

            validWeight = aux;

            if (VisitNode(valid))
            {
                return path;
            }  
        }
        return null;
    }

    public override void Clear()
    {
        base.Clear();
        openNodesList.Clear();
        validWeight = 0;
        valid = null;

    }

    public override void OpenNode(Node node)
    {
        openNodesList.Add(node);
    }

    public override void CloseNode(Node node)
    {
        base.CloseNode(node);
        openNodesList.Remove(node);
    }
}

