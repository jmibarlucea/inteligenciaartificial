﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreadthFirst : Pathfinder {

    public override bool VisitNode(Node node)
    {  
        if (node == endNode)
        {
            SetPath(node);
            return true;
        }


        foreach (Node n in node.Adj)
        {
            if (!openNodes.Contains(n) && !closedNodes.Contains(n) && !n.isUsed)
            {
                n.setParent(node);
                OpenNode(n);
            }
        }

        CloseNode(node);

        return false;
    }
}
