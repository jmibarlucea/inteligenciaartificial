﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder{

    public Queue<Node> openNodes = new Queue<Node>();
    public List<Node> closedNodes = new List<Node>();
    public Stack<Node> path = new Stack<Node>();
    public Node endNode;
    public Node startNode;


    public virtual void OpenNode(Node node)
    {
        openNodes.Enqueue(node);
    }
    
    public virtual void CloseNode(Node node)
    {
        closedNodes.Add(node);
    }

    public virtual void SetPath(Node node)
    {
        Node aux = node;
        while(aux.father)
        {
            path.Push(aux);
            aux = aux.father;
        }
        //Grid.instance.Clear();
    }

    public virtual bool VisitNode(Node node)
    {
        return true;
    }


    public virtual Stack<Node> FindPath(Node start, Node end)
    {
        Clear();
        startNode = start;
        endNode = end;
        OpenNode(start);

        while (openNodes.Count > 0)
        {
            if (VisitNode(GetFirst()))
            {
                return path;
            }
        }
        return null;
    }

    public virtual void Clear()
    {
        openNodes.Clear();
        closedNodes.Clear();
        Grid.instance.Clear();
    }

    public virtual Node GetFirst()
    {
        return openNodes.Dequeue();
    }
}
