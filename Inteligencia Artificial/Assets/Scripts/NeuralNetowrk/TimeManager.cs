﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public TextMeshProUGUI timeText;

    public void SpeedUp()
    {
        Time.timeScale *= 2;
    }

    public void NormalTime()
    {
        Time.timeScale = 1;
    }

    private void Update()
    {
        timeText.text = "x" + Time.timeScale;
    }
}
