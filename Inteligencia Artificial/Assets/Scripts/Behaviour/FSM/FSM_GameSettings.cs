﻿using UnityEngine;

public class FSM_GameSettings : MonoBehaviour
{

    public static FSM_GameSettings Instance { get; private set; }


    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);
        else
            Instance = this;
    }

    //[SerializeField] private float workerSpeed = 2f;
    //public static float WorkerSpeed => Instance.workerSpeed;

    //[SerializeField] private float findRadius = 4f;
    //public static float FindRadius = Instance.findRadius;

    //[SerializeField] private float collectRange = 1f;
    //public static float CollectRange = Instance.collectRange;
}
