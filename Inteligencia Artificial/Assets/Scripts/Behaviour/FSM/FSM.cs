﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FSM : MonoBehaviour
{
    private Dictionary<Type, BaseState> availableStates;

    public BaseState CurrentState { get; private set; }
    public event Action<BaseState> OnStateChanged;

    public Text stateText;

    private void Update()
    {
        if(CurrentState == null)
        {
            CurrentState = availableStates.Values.First();
            stateText.text = CurrentState?.GetType().ToString();
        }

        var nextState = CurrentState?.Tick();

        if(nextState != null && nextState != CurrentState?.GetType())
        {
            SwitchToNewState(nextState);
        }
    }

    private void SwitchToNewState(Type nextState)
    {
        CurrentState = availableStates[nextState];
        OnStateChanged?.Invoke(CurrentState);
        stateText.text = CurrentState?.GetType().ToString();
    }

    public void SetStates(Dictionary<Type,BaseState> states)
    {
        availableStates = states;
    }
}
