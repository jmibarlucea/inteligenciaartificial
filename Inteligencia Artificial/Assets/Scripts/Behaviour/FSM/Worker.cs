﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Worker : MonoBehaviour
{

    public Transform Home { get; private set; }
    public Transform Target { get; private set; }


    public Dropdown dropdown;

    public Text BagText;
    public int BagCapacity { get; set; } = 5;
    public int CurrentBagAmount { get; set; } = 0;

    private FSM stateMachine => GetComponent<FSM>();
    private BTree tree => GetComponent<BTree>();

    

    void Awake()
    {
        InitializeStateMachine();
        InitializeTree();
        Home = GameObject.FindGameObjectWithTag("Home").transform;   
    }

 

    private void Update()
    {
        BagText.text =  IsBagFull() ? "Bag: FULL!" : "Bag: " + CurrentBagAmount;
    }

    private void InitializeStateMachine()
    {
        var states = new Dictionary<Type, BaseState>()
        {
            { typeof(WanderState), new WanderState(this)},
            { typeof(ChaseState), new ChaseState(this)},
            { typeof(CollectState), new CollectState(this)},
            { typeof(UnloadState), new UnloadState(this)}

        };

        stateMachine.SetStates(states);
    }

    private void InitializeTree()
    {

        var sequenceList = new List<Node_BT>();
        sequenceList.Add(new WanderAction(this));
        sequenceList.Add(new ChaseAction(this));
        sequenceList.Add(new CollectAction(this));
        sequenceList.Add(new ChaseAction(this));
        sequenceList.Add(new UnloadAction(this));

        var sequence = new Sequence(sequenceList);

        tree.SetTree(sequence);
    }


    public void SetTarget(Transform target)
    {
        Target = target;
    }

    public bool IsBagFull()
    {
        return CurrentBagAmount >= BagCapacity;
    }

    public void SwitchBehav()
    {
        stateMachine.enabled =  !stateMachine.enabled;
        tree.enabled = !tree.enabled;
    }
}
