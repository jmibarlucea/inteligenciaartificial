﻿using System;
using UnityEngine;

public class UnloadState : BaseState
{
    Worker worker;

    float elapsedTime = 0f;
    float unloadTime = .5f;

    public UnloadState(Worker worker) : base(worker.gameObject)
    {
        this.worker = worker;
    }

    public override Type Tick()
    {

        if(worker.CurrentBagAmount <= 0)
        {
            return typeof(WanderState);
        }

        Unload();
        elapsedTime += Time.deltaTime;

        return null;
    }

    private void Unload()
    {
        if (elapsedTime >= unloadTime)
        {
            worker.CurrentBagAmount--;
            elapsedTime = 0;
        }
    }
}
