﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

public class ChaseState : BaseState
{

    private Worker worker;

    public ChaseState(Worker worker) : base(worker.gameObject)
    {
        this.worker = worker;
    }

    public override Type Tick()
    {
        if(worker.Target == null)
        {
            return typeof(WanderState);
        }

        var workerSpeed = 6f;

        transform.LookAt(worker.Target);
        transform.Translate(Vector3.forward * Time.deltaTime * workerSpeed);

        var distance = Vector3.Distance(transform.position, worker.Target.transform.position);
        var interactRange = 3f;

        if(distance <= interactRange)
        {
            if (worker.IsBagFull())
                return typeof(UnloadState);
            else
                return typeof(CollectState);
        }

        return null;
    }
}
