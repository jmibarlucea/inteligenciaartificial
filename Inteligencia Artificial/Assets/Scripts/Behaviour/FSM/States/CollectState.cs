﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class CollectState : BaseState
{
    private Worker worker;

    float elapsedTime = 0f;
    float collectTime = .5f;


    public CollectState(Worker worker) : base(worker.gameObject) 
    {
        this.worker = worker;
    }

    public override Type Tick()
    {
        if (worker.IsBagFull())
        {
            worker.SetTarget(worker.Home);
            return typeof(ChaseState);
        }
        else if (worker.Target == null)
        {
            return typeof(WanderState);
        }

        Collect();
        elapsedTime += Time.deltaTime;

        return null;
    }

    private void Collect()
    {
        if(elapsedTime >= collectTime)
        {
            var resource = worker.Target.GetComponent<Resource>();
            resource.MaterialQuantity--;
            worker.CurrentBagAmount++;
            elapsedTime = 0;
        }
    }


}
