﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resource : MonoBehaviour
{

    private int quant;
    public int MaterialQuantity { get { return quant; } set { quant = value; UpdateText(); } }
    public Text quantityText;

    void Start()
    {
        MaterialQuantity = Random.Range(1, 10);
    }

    void Update()
    {
        if(MaterialQuantity <= 0)
        {
            Destroy(gameObject);
        }
    }

    void UpdateText()
    {
        quantityText.text = "Ore: " + MaterialQuantity;
    }
}
