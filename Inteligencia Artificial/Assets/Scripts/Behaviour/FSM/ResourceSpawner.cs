﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ResourceSpawner : MonoBehaviour
{

    public GameObject ResourcePrefab;
    
    [SerializeField] private List<GameObject> resourceList;


    private void Start()
    {
        StartCoroutine("SpawnResource");

    }

    IEnumerator SpawnResource()
    {
        var spawnTime = UnityEngine.Random.Range(3, 10);

        yield return new WaitForSeconds(spawnTime);

        var spawnPosition = GetNewPosition();

        resourceList = resourceList.Where(item => item != null).ToList();

        if (resourceList.Count < 5)
        {
            resourceList.Add(Instantiate(ResourcePrefab, spawnPosition, Quaternion.identity));
        }

        StartCoroutine("SpawnResource");

    }

    private Vector3 GetNewPosition()
    {

        var position = UnityEngine.Random.insideUnitSphere * 10;

        while(Physics.OverlapSphere(position, 5f).Length > 0)
        {
            position = UnityEngine.Random.insideUnitSphere * 10;
        }

        position.Set(position.x, 1, position.z);

        return position;

    }

}
