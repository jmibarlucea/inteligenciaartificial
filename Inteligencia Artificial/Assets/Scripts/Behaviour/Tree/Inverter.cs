﻿using UnityEngine;
using System.Collections;

public class Inverter : Node_BT
{
    Node_BT m_node;

    public Node_BT node
    {
        get { return m_node; }
    }

  
    public Inverter(Node_BT node)
    {
        m_node = node;
    }


    public override NodeStates Evaluate()
    {
        switch (m_node.Evaluate())
        {
            case NodeStates.FAILURE:
                m_nodeState = NodeStates.SUCCESS;
                return m_nodeState;
            case NodeStates.SUCCESS:
                m_nodeState = NodeStates.FAILURE;
                return m_nodeState;
            case NodeStates.RUNNING:
                m_nodeState = NodeStates.RUNNING;
                return m_nodeState;
        }
        m_nodeState = NodeStates.SUCCESS;
        return m_nodeState;
    }
}