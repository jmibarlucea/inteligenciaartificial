﻿using UnityEngine;

public class ChaseAction : Node_BT
{

    public Worker worker;

    public ChaseAction(Worker _worker)
    {
        worker = _worker;
    }

    public override NodeStates Evaluate()
    {

        if(worker.Target == null)
        {
            return NodeStates.FAILURE;
        }

        var distance = Vector3.Distance(worker.transform.position, worker.Target.transform.position);
        var interactRange = 3f;

        if (distance <= interactRange)
        {
            return NodeStates.SUCCESS;
        }

        var workerSpeed = 6f;

        worker.transform.LookAt(worker.Target);
        worker.transform.Translate(Vector3.forward * Time.deltaTime * workerSpeed);

        return NodeStates.FAILURE;
    }
}
