﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderAction : Node_BT
{
    private Vector3? destination;
    private float stopDistance = 1f;
    private float turnSpeed = 4f;
    private readonly LayerMask layerMask = LayerMask.NameToLayer("Walls");
    private float findRadius = 3f;
    private Quaternion desiredRotation;
    private Vector3 direction;

    private Worker worker;

    public WanderAction(Worker _worker)
    {
        worker = _worker;
    }

    public override NodeStates Evaluate()
    {
        var chaseTarget = CheckForResource();
        if (chaseTarget != null)
        {
            worker.SetTarget(chaseTarget);
            return NodeStates.SUCCESS;
        }

        if (destination.HasValue == false || Vector3.Distance(worker.transform.position, destination.Value) <= stopDistance)
        {
            FindRandomDestination();
        }

        worker.transform.rotation = Quaternion.Slerp(worker.transform.rotation, desiredRotation, Time.deltaTime * turnSpeed);

        while (IsPathBlocked())
        {
            FindRandomDestination();
        }


        var workerSpeed = 4f;
        worker.transform.Translate(Vector3.forward * Time.deltaTime * workerSpeed);


        return NodeStates.FAILURE;
    }

    private bool IsPathBlocked()
    {
        RaycastHit hit;
        if (Physics.Raycast(worker.transform.position, direction, out hit, findRadius, layerMask))
        {
            Debug.DrawRay(worker.transform.position, direction * hit.distance, Color.blue);
            return true;
        }

        return false;
    }

    private void FindRandomDestination()
    {
        Vector3 testPosition = (worker.transform.position + worker.transform.forward * 2f) + new Vector3(UnityEngine.Random.Range(-4.4f, 4.5f), 1f, UnityEngine.Random.Range(-4.4f, 4.5f));

        destination = new Vector3(testPosition.x, 1f, testPosition.z);
        direction = Vector3.Normalize(destination.Value - worker.transform.position);
        desiredRotation = Quaternion.LookRotation(direction);
    }

    Quaternion startingAngle = Quaternion.AngleAxis(-60, Vector3.up);
    Quaternion stepAngle = Quaternion.AngleAxis(5, Vector3.up);

    private Transform CheckForResource()
    {
        if (worker.Target != null)
        {
            return worker.Target;
        }

        RaycastHit hit;


        var angle = worker.transform.rotation * startingAngle;
        var direction = angle * Vector3.forward;
        var pos = worker.transform.position;

        for (var i = 0; i < 24; i++)
        {
            if (Physics.Raycast(pos, direction, out hit, findRadius * 2))
            {
                var resource = hit.collider.GetComponent<Resource>();
                if (resource != null)
                {
                    //Debug.DrawRay(pos, direction * hit.distance, Color.red);
                    return resource.transform;
                }
                else
                {
                    //Debug.DrawRay(pos, direction * hit.distance, Color.green);

                }
            }
            else
            {
                Debug.DrawRay(pos, direction * hit.distance, Color.white);
            }
            direction = stepAngle * direction;
        }

        return null;
    }
}
