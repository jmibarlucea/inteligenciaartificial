﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectAction : Node_BT
{
    private Worker worker;

    float elapsedTime = 0f;
    float collectTime = .5f;


    public CollectAction(Worker worker)
    {
        this.worker = worker;
    }

    public override NodeStates Evaluate()
    {
        if(worker.Target == worker.Home)
        {
            return NodeStates.SUCCESS;
        }

        if (worker.IsBagFull())
        {
            worker.SetTarget(worker.Home);
            return NodeStates.SUCCESS;
        }
        
        if (worker.Target == null)
        {
            return NodeStates.FAILURE;
        }

        Collect();
        elapsedTime += Time.deltaTime;

        return NodeStates.FAILURE;
    }

    private void Collect()
    {
        if (elapsedTime >= collectTime)
        {
            var resource = worker.Target.GetComponent<Resource>();
            resource.MaterialQuantity--;
            worker.CurrentBagAmount++;
            elapsedTime = 0;
        }
    }
}
