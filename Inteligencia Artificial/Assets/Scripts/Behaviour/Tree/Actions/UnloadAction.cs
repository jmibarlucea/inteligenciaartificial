﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnloadAction : Node_BT
{

    Worker worker;

    float elapsedTime = 0f;
    float unloadTime = .5f;

    public UnloadAction(Worker worker)
    {
        this.worker = worker;
    }

    public override NodeStates Evaluate()
    {

        if (worker.CurrentBagAmount <= 0)
        {
            worker.SetTarget(null);
            return NodeStates.SUCCESS;
        }

        Unload();
        elapsedTime += Time.deltaTime;

        return NodeStates.FAILURE;
    }

    private void Unload()
    {
        if (elapsedTime >= unloadTime)
        {
            worker.CurrentBagAmount--;
            elapsedTime = 0;
        }
    }
}
