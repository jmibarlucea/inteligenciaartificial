﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class Sequence : Node_BT
{
    public List<Node_BT> m_nodes = new List<Node_BT>();
    public Node_BT currentNode;

    public Sequence(List<Node_BT> nodes)
    {
        m_nodes = nodes;
    }

    public override NodeStates Evaluate()
    {
        bool anyChildRunning = false;

        foreach (Node_BT node in m_nodes)
        {
            switch (node.Evaluate())
            {
                case NodeStates.FAILURE:
                    m_nodeState = NodeStates.FAILURE;
                    currentNode = node;
                    return m_nodeState;
                case NodeStates.SUCCESS:
                    continue;
                case NodeStates.RUNNING:
                    anyChildRunning = true;
                    continue;
                default:
                    m_nodeState = NodeStates.SUCCESS;
                    return m_nodeState;
            }
        }
        m_nodeState = anyChildRunning ? NodeStates.RUNNING : NodeStates.SUCCESS;
        return m_nodeState;
    }
}