﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class BTree : MonoBehaviour
{

    public Text stateText;
    private Node_BT CurrentState;

    private void Update()
    {
        CurrentState.Evaluate();

        var sequence = ((Sequence)CurrentState);
        stateText.text = sequence.currentNode != null ? sequence.currentNode.GetType().ToString() : "Running";
    }

    public void SetTree(Node_BT _tree)
    {
        CurrentState = _tree;
    }
}
