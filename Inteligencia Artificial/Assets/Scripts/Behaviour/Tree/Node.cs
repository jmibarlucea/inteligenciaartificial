﻿using UnityEngine;
using System.Collections;


public enum NodeStates
{
    SUCCESS,
    FAILURE,
    RUNNING
}

[System.Serializable]
public abstract class Node_BT
{

    public delegate NodeStates NodeReturn();

    protected NodeStates m_nodeState;

    public NodeStates nodeState
    {
        get { return m_nodeState; }
    }

    public Node_BT() { }

    public abstract NodeStates Evaluate();

}